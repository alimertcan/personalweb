from django.db import models
import os
from django.conf import settings


class Developer(models.Model):
    fist_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phonenumber = models.CharField(blank=True, max_length=13)
    skills = models.TextField()
    about = models.TextField()

    def __str__(self):
        return '{} {}'.format(self.fist_name, self.last_name)


class Resume(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    file = models.FileField(upload_to='cv')
    title = models.CharField(blank=True, null=True, max_length=55)

    def __str__(self):
        return '{}'.format(self.title)


class WorkExperience(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    companyname = models.CharField(max_length=255)
    explanation = models.CharField(max_length=255)
    date_start = models.DateField()
    date_end = models.DateField(blank=True, null=True)

    def __str__(self):
        return '{} as {}'.format(self.companyname, self.title)


class Project(models.Model):
    title = models.CharField(max_length=255)
    link = models.URLField()
    explanation = models.CharField(max_length=255)
    image = models.ImageField(upload_to='static/images', blank=True)
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)

    def __str__(self):
        return '{} '.format(self.title)


class Education(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    nameofschool = models.CharField(max_length=255)
    date = models.DateField()
    title = models.CharField(max_length=255)
    courses = models.TextField()

    def __str__(self):
        return '{} '.format(self.nameofschool)

    def courses_as_list(self):
        return self.courses.rsplit(',')


class Contact(models.Model):
    message = models.TextField()
    email = models.CharField(max_length=255)
    date=models.DateTimeField(blank=True,null=True)

    def __str__(self):
        return self.email
