# Generated by Django 3.1.7 on 2021-03-07 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mywebsite', '0007_auto_20210307_2033'),
    ]

    operations = [
        migrations.AddField(
            model_name='resume',
            name='title',
            field=models.CharField(blank=True, max_length=55, null=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='file',
            field=models.FileField(upload_to='cv'),
        ),
    ]
