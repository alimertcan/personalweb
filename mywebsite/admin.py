from django.contrib import admin
from .models import Project, Developer, Education, WorkExperience, Resume, Contact

# Register your models here.
admin.site.register(Project)
admin.site.register(Developer)
admin.site.register(Education)
admin.site.register(WorkExperience)
admin.site.register(Resume)
admin.site.register(Contact)
