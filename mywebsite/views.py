from django.shortcuts import render, redirect
from .models import *
from .forms import ContactForm
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, Http404
import datetime


def index(request):
    if request.method == 'GET':
        devobj = Developer.objects.get(pk=1)
        project = devobj.project_set.all()
        education = devobj.education_set.all()
        resume = devobj.resume_set.get(pk=1)
        workexp = devobj.workexperience_set.all()
        skills = devobj.skills.rsplit(',')
        form = ContactForm()

        context = {
            "fullname": devobj.__str__(),
            "about": devobj.about,
            "skills": skills,
            "phonenumber": devobj.phonenumber,
            "project": project,
            "education": education,
            "workexp": workexp,
            'form': form,
            'resume': resume.file
        }

        return render(request, 'index.html', context)

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = "Sitedeki Contact Form"
            body = {
                'email': form.cleaned_data['email_address'],
                'message': form.cleaned_data['message'],
            }
            message = "\n".join(body.values())
            con = Contact(email=body['email'], message=message,date=datetime.datetime.now())

            try:
                sender="alimertcantekin@gmail.com"
                recipients=["tekin.mertcan@yahoo.com"]
                con.save()
                send_mail(subject,message,sender,recipients,fail_silently=True)
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect("index")


