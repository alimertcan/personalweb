from django import forms

# Create your forms here.

class ContactForm(forms.Form):
    email_address = forms.EmailField(max_length = 150)
    message = forms.CharField(widget = forms.Textarea, max_length = 2000)